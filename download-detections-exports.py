import argparse
import glob
import os
import pathlib
import requests
import uuid
import zipfile
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta


# request this from support@satelligence.com
SATELLIGENCE_API_TOKEN = "<TOKEN>"

SATELLIGENCE_DETECTIONS_EXPORT_API_URL = "https://api.satelligence.com/detections-export"


def download_detections_for_month(month, year, dest_dir_path):
    dest_dir_path = pathlib.Path(dest_dir_path)
    results_root_folder_path = dest_dir_path.joinpath(
        'satelligence-detections-downloads')
    os.makedirs(results_root_folder_path, exist_ok=True)

    download_url = get_download_url_for_detections(month, year)
    if (not download_url):
        print(f'No export for month {month} year {year}: Skipping')
        return

    response = requests.get(download_url)
    response.raise_for_status()

    temp_zip_file_path = results_root_folder_path.joinpath(
        f'tmp-{uuid.uuid4()}.zip')
    with open(temp_zip_file_path, 'wb+') as f:
        f.write(response.content)

    results_folder_path = results_root_folder_path.joinpath(
        f'satelligence-detections-{year}-{month}')
    with zipfile.ZipFile(temp_zip_file_path, 'r') as zip_ref:
        zip_ref.extractall(results_folder_path)
    os.remove(temp_zip_file_path)
    
    print(f'Export for month {month} year {year}: Complete')


def get_download_url_for_detections(month, year):
    headers = {'Authorization': f'Bearer {SATELLIGENCE_API_TOKEN}'}
    # monthIndex is 0-based i.e March is 2 and December is 11
    params = {'monthIndex': month - 1, "year": year}

    try:
        response = requests.get(
            SATELLIGENCE_DETECTIONS_EXPORT_API_URL, headers=headers, params=params)
        response.raise_for_status()

        download_url = response.json()['downloadUrl']
        return download_url
    except requests.exceptions.HTTPError as error:
        print(error)
        print(error.response.text)
        print(response.json())
        raise
        

def get_num_months(start_date, end_date):
    return (end_date.year - start_date.year) * 12 + \
end_date.month - start_date.month

  
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--start',
                        type=str,
                        required=False,
                        default=str(date.today() - relativedelta(months=6))[:-3])
    parser.add_argument('--end',
                        type=str,
                        required=False,
                        default=str(date.today())[:-3])
    args = parser.parse_args()
    start_date = datetime.strptime(args.start,'%Y-%m').date()
    end_date = datetime.strptime(args.end,'%Y-%m').date()
    
    if (end_date - start_date).days < 0:
        print(f'End date falls before the start date, please adjust.')
        exit()
    
    num_months = get_num_months(start_date, end_date)
    export_dates = [start_date + relativedelta(months=i) for i in range(0,num_months+1)]
    for export_date in export_dates:
        download_detections_for_month(export_date.month, export_date.year, os.getcwd())

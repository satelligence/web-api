# Satelligence web API
Fetch detections export download urls for a given year and month

## Authentication
The API uses token authentication. Please contact [support](mailto:support@satelligence.com) to request a token. The token is connected to a personal account, to create an account ask one of you colleagues to create an invite from app.satelligence.com/settings or contact [support](mailto:support@satelligence.com).

Unauthenticated requests return:

```
Unauthorized
```

To authenticate provide the token as an `Authorization: Bearer <token>`

## Endpoints
Satelligence web API supports one endpoint available at: `https://api.satelligence.com/detections-export`

### Parameters
The endpoint takes 2 required parameters or returns a list of missing parameters:

```json
[
  {
    "code": "invalid_type",
    "expected": "number",
    "received": "undefined",
    "path": [
      "monthIndex"
    ],
    "message": "Required"
  },
  {
    "code": "invalid_type",
    "expected": "number",
    "received": "undefined",
    "path": [
      "year"
    ],
    "message": "Required"
  }
]
```

1. monthIndex: a 0 based index of the requested month. Example December: `monthIndex=11`
2. year: the year in yyyy format. Example 2021: `year=2021`

### Response
Succesfull responses are JSON with one attribute downloadUrl of type string or null for unavailable. Example unavailable: 

```json
{"downloadUrl": null}
```

Available:

```json
{"downloadUrl": "https://storage.googleapis.com/satelligence-app.appspot.com/file-to-download"}
```

### Example request
To get a download url for December 2021

```bash
curl --header "Authorization: Bearer <token>" "https://api.satelligence.com/detections-export?year=2021&monthIndex=11"
```

See the python script included in this repo for a full fledged example in python.

## Download file
The files can be fetched using the download url. Using cURL:

```bash
curl -o <your_filename> <downloadUrl>
```

### Format and content
The file is a zip compressed file of shapefile `deforestation.*` detections (if any) and `fire.*` detections (if any). 

Deforestation shapefiles have the following attributes:

* detection id
* event id
* date
* attribute per noncompliance area (ha)
* attribute for the area not overlapping with any special area (ha)
* attribute for the total area (ha)


### Using the python script
It is also possible to download a set of monthly detections using the download-detections-export script. This script accepts start and end dates to download a specific time range of monthly detections.

Specifying a date range is optional. By default the script will return the last 6 months of deforestation detections.

Command to use the Python script without dates:

```bash
python3 download-detections-exports.py
```

Command to use the Python script to download a specific date range:

```bash
python3 download-detections-exports.py --start <YYYY-MM> --end <YYYY-MM>
```
